/*
 * Copyright (C) 1997-2009 Andre Noll <maan@tuebingen.mpg.de>
 *
 * Licensed under the GPL v2. For licencing details see COPYING.
 */

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h> /* time(), localtime() */
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <ctype.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/un.h> /* needed by create_pf_socket */
#include <string.h>
#include <assert.h>
#include "gcc-compat.h"

/** debug loglevel, gets really noisy */
#define DEBUG 1
/** still noisy, but won't fill your disk */
#define INFO  2
/** normal, but significant event */
#define NOTICE 3
/** unexpected event that can be handled */
#define WARNING 4
/** unhandled error condition */
#define ERROR 5
/** system might be unreliable */
#define CRIT 6
/** last message before exit */
#define EMERG 7

/** Log messages with lower priority than that will not be compiled in. */
#define COMPILE_TIME_LOGLEVEL 0

/** \cond */
#if DEBUG > COMPILE_TIME_LOGLEVEL
#define DEBUG_LOG(f,...) __log(DEBUG, "%s: " f, __FUNCTION__, ## __VA_ARGS__)
#else
#define DEBUG_LOG(...) do {;} while (0)
#endif

#if INFO > COMPILE_TIME_LOGLEVEL
#define INFO_LOG(f,...) __log(INFO, "%s: " f, __FUNCTION__, ## __VA_ARGS__)
#else
#define INFO_LOG(...) do {;} while (0)
#endif

#if NOTICE > COMPILE_TIME_LOGLEVEL
#define NOTICE_LOG(f,...) __log(NOTICE, "%s: " f, __FUNCTION__, ## __VA_ARGS__)
#else
#define NOTICE_LOG(...) do {;} while (0)
#endif

#if WARNING > COMPILE_TIME_LOGLEVEL
#define WARNING_LOG(f,...) __log(WARNING, "%s: " f, __FUNCTION__, ##  __VA_ARGS__)
#else
#define WARNING_LOG(...) do {;} while (0)
#endif

#if ERROR > COMPILE_TIME_LOGLEVEL
#define ERROR_LOG(f,...) __log(ERROR, "%s: " f, __FUNCTION__, ## __VA_ARGS__)
#else
#define ERROR_LOG(...) do {;} while (0)
#endif

#if CRIT > COMPILE_TIME_LOGLEVEL
#define CRIT_LOG(f,...) __log(CRIT, "%s: " f, __FUNCTION__, ## __VA_ARGS__)
#else
#define CRIT_LOG(...) do {;} while (0)
#endif

#if EMERG > COMPILE_TIME_LOGLEVEL
#define EMERG_LOG(f,...) __log(EMERG, "%s: " f, __FUNCTION__, ## __VA_ARGS__)
#else
#define EMERG_LOG(...)
#endif
/** \endcond */

__printf_2_3 void __log(int, const char*, ...);
