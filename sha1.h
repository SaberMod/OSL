/** \file sha1.h Secure Hash Algorithm prototype */

/** Size of the hash value in bytes. */
#define HASH_SIZE 20
void sha1_hash(const char *data, unsigned long len, unsigned char *sha1);
