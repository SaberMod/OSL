/*
 * Copyright (C) 2007-2009 Andre Noll <maan@tuebingen.mpg.de>
 *
 * Licensed under the GPL v2. For licencing details see COPYING.
 */

/** \file hash.h Inline functions for hash values. */

#include "portable_io.h"

/** hash arrays are always unsigned char. */
#define HASH_TYPE unsigned char

#include "sha1.h"
/** Our own sha1 implementation, see sha1.c. */
#define hash_function sha1_hash

/**
 * Compare two hashes.
 *
 * \param h1 Pointer to the first hash value.
 * \param h2 Pointer to the second hash value.
 *
 * \return 1, -1, or zero, depending on whether \a h1 is greater than,
 * less than or equal to h2, respectively.
 */
_static_inline_ int hash_compare(HASH_TYPE *h1, HASH_TYPE *h2)
{
	int i;

	for (i = 0; i < HASH_SIZE; i++) {
		if (h1[i] < h2[i])
			return -1;
		if (h1[i] > h2[i])
			return 1;
	}
	return 0;
}

/**
 * Convert a hash value to ascii format.
 *
 * \param hash the hash value.
 * \param asc Result pointer.
 *
 * \a asc must point to an area of at least 2 * \p HASH_SIZE + 1 bytes which
 * will be filled by the function with the ascii representation of the hash
 * value given by \a hash, and a terminating \p NULL byte.
 */
_static_inline_ void hash_to_asc(HASH_TYPE *hash, char *asc)
{
	int i;
	const char hexchar[] = "0123456789abcdef";

	for (i = 0; i < HASH_SIZE; i++) {
		asc[2 * i] = hexchar[hash[i] >> 4];
		asc[2 * i + 1] = hexchar[hash[i] & 0xf];
	}
	asc[2 * HASH_SIZE] = '\0';
}
