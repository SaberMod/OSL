/*
 * Copyright (C) 2006-2009 Andre Noll <maan@tuebingen.mpg.de>
 *
 * Licensed under the GPL v2. For licencing details see COPYING.
 */

/** \file util.h exported functions from util.c */

int osl_open(const char *path, int flags, mode_t mode);
int mmap_full_file(const char *filename, int open_mode, void **map,
	size_t *size, int *fd_ptr);
int osl_munmap(void *start, size_t length);
int write_all(int fd, const char *buf, size_t *len);
int write_file(const char *filename, const void *buf, size_t size);
int truncate_file(const char *filename, off_t size);

/**
 * A wrapper for mkdir(2).
 *
 * \param path Name of the directory to create.
 * \param mode The permissions to use.
 *
 * \return Standard.
 */
static inline int osl_mkdir(const char *path, mode_t mode)
{
	if (!mkdir(path, mode))
		return 1;
	return errno == EEXIST? -E_OSL_DIR_EXISTS : -E_OSL_MKDIR;
}

/**
 * A wrapper for rename(2).
 *
 * \param old_path The source path.
 * \param new_path The destination path.
 *
 * \return Standard.
 *
 * \sa rename(2).
 */
_static_inline_ int osl_rename(const char *old_path, const char *new_path)
{
	if (rename(old_path, new_path) < 0)
		return -E_OSL_RENAME;
	return 1;
}

_static_inline_ int osl_stat(const char *path, struct stat *buf)
{
	if (stat(path, buf) >= 0)
		return 1;
	return errno == ENOENT? -E_OSL_NOENT : -E_OSL_STAT;
}
